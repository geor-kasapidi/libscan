//
//  N7NavigationController.h
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface N7NavigationController : UINavigationController

@end
