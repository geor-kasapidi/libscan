
//
//  N7ViewPreferredActions.h
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol N7ViewPreferredActions <NSObject>

@optional

- (void)setupUI;
- (void)bindUI;
- (void)localizeUI;

@end
