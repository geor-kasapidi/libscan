//
//  N7BooksFileSystemManager.m
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

@import SSZipArchive;

#import <UIKit/UIImage.h>

#import "N7BooksFileSystemManager.h"
#import "N7BookModel.h"
#import "N7PersistentUUIDArray.h"

#import "NSString+N7Helpers.h"

static NSString * const N7BooksDefaultsDirName = @"Books";
static NSString * const N7BookListFileName = @"books";
static NSString * const N7BookListFileExtension = @"txt";
static NSString * const N7BookDataFileName = @"data";
static NSString * const N7BookDataFileExtension = @"txt";
static NSString * const N7BookContentFileName = @"content";
static NSString * const N7BookContentFileExtensions = @"txt";
static NSString * const N7OriginalPhotosDirName = @"Original";
static NSString * const N7PreviewPhotosDirName = @"Preview";
static NSString * const N7OriginalPhotoFileExtension = @"jpg";
static NSString * const N7PreviewPhotoFileExtension = @"jpg";

static const CGFloat N7PhotoJPEGCompression = 0.5;

@interface N7BooksFileSystemManager ()

@property (copy, nonatomic) NSString *booksDir;

@end

@implementation N7BooksFileSystemManager

+ (instancetype)manager {
    static N7BooksFileSystemManager *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [N7BooksFileSystemManager new];
    });
    
    return instance;
}

- (instancetype)init {
    NSString *docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *booksDir = [docsDir stringByAppendingPathComponent:N7BooksDefaultsDirName];
    
    if (![[NSFileManager defaultManager] createDirectoryAtPath:booksDir withIntermediateDirectories:YES attributes:nil error:nil]) {
        return nil;
    }
    
    self = [super init];
    
    if (self) {
        self.booksDir = booksDir;
    }
    
    return self;
}

#pragma mark - Public

- (BOOL)createOrUpdateBook:(N7BookModel *)book {
    NSParameterAssert(book.name.length > 0);
    NSParameterAssert(book.user.length > 0);
    
    NSUUID *bookID = book.bookID ?: [NSUUID UUID];
    
    NSString *bookDirPath = [self.booksDir stringByAppendingPathComponent:[bookID UUIDString]];
    
    if ([[NSFileManager defaultManager] createDirectoryAtPath:bookDirPath withIntermediateDirectories:YES attributes:nil error:nil]) {
        NSString *bookDataFilePath = [[bookDirPath stringByAppendingPathComponent:N7BookDataFileName] stringByAppendingPathExtension:N7BookDataFileExtension];
        
        NSString *bookDataContent = [NSString stringWithFormat:@"%@\n%@\n", book.name, book.user];
        
        if ([bookDataContent writeToFile:bookDataFilePath atomically:YES encoding:NSUTF8StringEncoding error:nil]) {
            NSString *originalPhotosDirPath = [bookDirPath stringByAppendingPathComponent:N7OriginalPhotosDirName];
            NSString *previewPhotosDirPath = [bookDirPath stringByAppendingPathComponent:N7PreviewPhotosDirName];
            
            if ([[NSFileManager defaultManager] createDirectoryAtPath:originalPhotosDirPath withIntermediateDirectories:YES attributes:nil error:nil] && [[NSFileManager defaultManager] createDirectoryAtPath:previewPhotosDirPath withIntermediateDirectories:YES attributes:nil error:nil]) {
                if (!book.bookID) {
                    [[self booksIDs] addID:bookID];
                    
                    book.bookID = bookID;
                }
                
                return YES;
            }
        }
    }
    
    return NO;
}

- (BOOL)deleteBook:(N7BookModel *)book {
    NSParameterAssert(book.bookID != nil);
    
    if (![[self booksIDs] deleteUUID:book.bookID]) {
        return NO;
    }
    
    NSString *bookDirPath = [self.booksDir stringByAppendingPathComponent:[book.bookID UUIDString]];
    
    return [[NSFileManager defaultManager] removeItemAtPath:bookDirPath error:nil];
}

- (NSArray<N7BookModel *> *)booksArray {
    NSMutableArray *models = [NSMutableArray array];
    
    NSArray<NSUUID *> *uuIDs = [[self booksIDs] uuIDs];
    
    [uuIDs enumerateObjectsUsingBlock:^(NSUUID * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        N7BookModel *book = [self bookModelWithID:obj];
        
        if (book) {
            [models addObject:book];
        }
    }];
    
    return [models copy];
}

- (N7BookModel *)findBookByID:(NSUUID *)bookID {
    NSArray<NSUUID *> *uuIDs = [[self booksIDs] uuIDs];
    
    if ([uuIDs containsObject:bookID]) {
        N7BookModel *book = [self bookModelWithID:bookID];
        
        return book;
    }
    
    return nil;
}

- (void)createBookPageWithOriginalImage:(UIImage *)originalImage andPreviewImage:(UIImage *)previewImage inBook:(N7BookModel *)book withCompletionHandler:(void (^)(NSUUID *))completionHandler {
    NSParameterAssert(originalImage != nil);
    NSParameterAssert(previewImage != nil);
    NSParameterAssert(book.bookID != nil);
    
    NSUUID *pageID = [NSUUID UUID];
    
    if (![[self bookPagesIDs:book] addID:pageID]) {
        if (completionHandler) {
            completionHandler(nil);
        }
        
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSString *previewImageFilePath = [[[[self.booksDir stringByAppendingPathComponent:[book.bookID UUIDString]] stringByAppendingPathComponent:N7PreviewPhotosDirName] stringByAppendingPathComponent:[pageID UUIDString]] stringByAppendingPathExtension:N7PreviewPhotoFileExtension];
        
        NSData *previewImageBytes = UIImageJPEGRepresentation(previewImage, N7PhotoJPEGCompression);
        
        if ([previewImageBytes writeToFile:previewImageFilePath atomically:YES]) {
            NSString *originalImageFilePath = [[[[self.booksDir stringByAppendingPathComponent:[book.bookID UUIDString]] stringByAppendingPathComponent:N7OriginalPhotosDirName] stringByAppendingPathComponent:[pageID UUIDString]] stringByAppendingPathExtension:N7OriginalPhotoFileExtension];
            
            NSData *originalImageBytes = UIImageJPEGRepresentation(originalImage, N7PhotoJPEGCompression);
            
            if ([originalImageBytes writeToFile:originalImageFilePath atomically:YES]) {
                if (completionHandler) {
                    completionHandler(pageID);
                }
                
                return;
            }
        }
        
        if (completionHandler) {
            completionHandler(nil);
        }
    });
}

- (BOOL)deletePageWithID:(NSUUID *)pageID inBook:(N7BookModel *)book {
    NSParameterAssert(pageID != nil);
    NSParameterAssert(book.bookID != nil);
    
    if (![[self bookPagesIDs:book] deleteUUID:pageID]) {
        return NO;
    }
    
    NSString *previewImageFilePath = [[[[self.booksDir stringByAppendingPathComponent:[book.bookID UUIDString]] stringByAppendingPathComponent:N7PreviewPhotosDirName] stringByAppendingPathComponent:[pageID UUIDString]] stringByAppendingPathExtension:N7PreviewPhotoFileExtension];
    
    if ([[NSFileManager defaultManager] removeItemAtPath:previewImageFilePath error:nil]) {
        NSString *originalImageFilePath = [[[[self.booksDir stringByAppendingPathComponent:[book.bookID UUIDString]] stringByAppendingPathComponent:N7OriginalPhotosDirName] stringByAppendingPathComponent:[pageID UUIDString]] stringByAppendingPathExtension:N7OriginalPhotoFileExtension];
        
        if ([[NSFileManager defaultManager] removeItemAtPath:originalImageFilePath error:nil]) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)moveFromCurrentIndexPageWithID:(NSUUID *)pageID toPosition:(NSUInteger)newIndex inBook:(N7BookModel *)book {
    NSParameterAssert(pageID != nil);
    NSParameterAssert(book.bookID != nil);
    
    return [[self bookPagesIDs:book] moveUUID:pageID toIndex:newIndex];
}

- (NSArray<NSUUID *> *)pagesIDsForBook:(N7BookModel *)book {
    return [[self bookPagesIDs:book] uuIDs];
}

- (UIImage *)pagePreviewImageWithID:(NSUUID *)pageID inBook:(N7BookModel *)book {
    NSParameterAssert(pageID != nil);
    NSParameterAssert(book.bookID != nil);
    
    NSString *previewImageFilePath = [[[[self.booksDir stringByAppendingPathComponent:[book.bookID UUIDString]] stringByAppendingPathComponent:N7PreviewPhotosDirName] stringByAppendingPathComponent:[pageID UUIDString]] stringByAppendingPathExtension:N7PreviewPhotoFileExtension];
    
    NSData *previewImageBytes = [NSData dataWithContentsOfFile:previewImageFilePath];
    
    if (previewImageBytes.length > 0) {
        UIImage *previewImage = [UIImage imageWithData:previewImageBytes];
        
        return previewImage;
    }
    
    return nil;
}

- (UIImage *)pageOriginalImageWithID:(NSUUID *)pageID inBook:(N7BookModel *)book {
    NSParameterAssert(pageID != nil);
    NSParameterAssert(book.bookID != nil);
    
    NSString *originalImageFilePath = [[[[self.booksDir stringByAppendingPathComponent:[book.bookID UUIDString]] stringByAppendingPathComponent:N7OriginalPhotosDirName] stringByAppendingPathComponent:[pageID UUIDString]] stringByAppendingPathExtension:N7OriginalPhotoFileExtension];
    
    NSData *originalImageBytes = [NSData dataWithContentsOfFile:originalImageFilePath];
    
    if (originalImageBytes.length > 0) {
        UIImage *originalImage = [UIImage imageWithData:originalImageBytes];
        
        return originalImage;
    }
    
    return nil;
}

- (NSString *)zipBookContent:(N7BookModel *)book {
    NSParameterAssert(book.bookID != nil);
    
    NSArray<NSUUID *> *pagesIDs = [[self bookPagesIDs:book] uuIDs];
    
    if (pagesIDs.count == 0) {
        return nil;
    }
    
    NSMutableArray<NSString *> *filesPaths = [NSMutableArray arrayWithCapacity:pagesIDs.count];
    
    [pagesIDs enumerateObjectsUsingBlock:^(NSUUID * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *filePath = [[[[self.booksDir stringByAppendingPathComponent:[book.bookID UUIDString]] stringByAppendingPathComponent:N7OriginalPhotosDirName] stringByAppendingPathComponent:[obj UUIDString]] stringByAppendingPathExtension:N7OriginalPhotoFileExtension];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [filesPaths addObject:filePath];
        }
    }];
    
    if (filesPaths.count == 0) {
        return nil;
    }
    
    NSString *tempDir = [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:tempDir withIntermediateDirectories:YES attributes:nil error:nil];
    
    [filesPaths enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *tempFilePath = [[tempDir stringByAppendingPathComponent:[@(idx + 1) stringValue]] stringByAppendingPathExtension:N7OriginalPhotoFileExtension];
        
        [[NSFileManager defaultManager] copyItemAtPath:obj toPath:tempFilePath error:nil];
    }];
    
    NSString *bookName = [[[book.name n7_translit] n7_alpanumWithJoiner:@"_"] lowercaseString];
    NSString *userName = [[[book.user n7_translit] n7_alpanumWithJoiner:@"_"] lowercaseString];
    
    NSString *zipName = [NSString stringWithFormat:@"%@_%@_%@", bookName ?: @"", userName ?: @"", @((NSUInteger)([NSDate date].timeIntervalSince1970))];
    
    NSString *zipFilePath = [[NSTemporaryDirectory() stringByAppendingPathComponent:zipName] stringByAppendingPathExtension:@"zip"];
    
    if ([SSZipArchive createZipFileAtPath:zipFilePath withContentsOfDirectory:tempDir]) {
        return zipFilePath;
    }
    
    return nil;
}

- (void)clearTempDir {
    NSString *tempDir = NSTemporaryDirectory();
    
    NSArray<NSString *> *tempDirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:tempDir error:nil];
    
    [tempDirContents enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *filePath = [tempDir stringByAppendingPathComponent:obj];
        
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }];
}

#pragma mark - Private

- (N7PersistentUUIDArray *)booksIDs {
    NSString *booksFilePath = [[self.booksDir stringByAppendingPathComponent:N7BookListFileName] stringByAppendingPathExtension:N7BookListFileExtension];
    
    N7PersistentUUIDArray *array = [[N7PersistentUUIDArray alloc] initWithFilePath:booksFilePath];
    
    return array;
}

- (N7BookModel *)bookModelWithID:(NSUUID *)bookID {
    NSString *bookDirPath = [self.booksDir stringByAppendingPathComponent:[bookID UUIDString]];
    
    NSString *bookDataFilePath = [[bookDirPath stringByAppendingPathComponent:N7BookDataFileName] stringByAppendingPathExtension:N7BookDataFileExtension];
    
    NSString *text = [NSString stringWithContentsOfFile:bookDataFilePath encoding:NSUTF8StringEncoding error:nil];
    
    NSArray<NSString *> *components = [text componentsSeparatedByString:@"\n"];
    
    if (components.count > 1) {
        NSString *bookName = [components[0] n7_trim];
        NSString *user = [components[1] n7_trim];
        
        if (bookName.length > 0 && user.length > 0) {
            N7BookModel *book = [N7BookModel modelWithID:bookID name:bookName user:user];
            
            return book;
        }
    }
    
    return nil;
}

- (N7PersistentUUIDArray *)bookPagesIDs:(N7BookModel *)book {
    NSString *bookContentFilePath = [[[self.booksDir stringByAppendingPathComponent:[book.bookID UUIDString]] stringByAppendingPathComponent:N7BookContentFileName] stringByAppendingPathExtension:N7BookContentFileExtensions];
    
    N7PersistentUUIDArray *array = [[N7PersistentUUIDArray alloc] initWithFilePath:bookContentFilePath];
    
    return array;
}

@end
