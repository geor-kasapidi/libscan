//
//  N7BookPhotoGalleryImageViewController.h
//  LibScan
//
//  Created by Георгий Касапиди on 19.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewController.h"

@class N7BookPhotoGalleryImageViewController;

@protocol N7BookPhotoGalleryImageViewControllerDelegate <NSObject>

@required

- (void)imageViewControllerDidAppear:(N7BookPhotoGalleryImageViewController *)imageViewController;

@end

@interface N7BookPhotoGalleryImageViewController : N7ViewController

@property (weak, nonatomic) id<N7BookPhotoGalleryImageViewControllerDelegate> delegate;

@property (assign, nonatomic, readonly) NSInteger photoIndex;

+ (instancetype)imageViewControllerWithImage:(UIImage *)image andIndex:(NSInteger)index;

@end
