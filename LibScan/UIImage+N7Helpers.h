//
//  UIImage+N7Helpers.h
//  LibScan
//
//  Created by Георгий Касапиди on 16.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (N7Helpers)

- (UIImage *)n7_previewWithSize:(CGSize)size;

@end
