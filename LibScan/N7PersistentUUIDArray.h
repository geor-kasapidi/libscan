//
//  N7PersistentUUIDArray.h
//  LibScan
//
//  Created by Георгий Касапиди on 18.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface N7PersistentUUIDArray : NSEnumerator<NSUUID *>

@property (nonatomic, readonly) NSArray<NSUUID *> *uuIDs;

- (instancetype)initWithFilePath:(NSString *)filePath;

- (BOOL)addID:(NSUUID *)uuID;
- (BOOL)deleteUUID:(NSUUID *)uuID;
- (BOOL)moveUUID:(NSUUID *)uuID toIndex:(NSUInteger)index;

@end
