//
//  NSString+N7Helpers.h
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (N7Helpers)

- (NSString *)n7_trim;
- (NSString *)n7_alpanumWithJoiner:(NSString *)joiner;
- (NSString *)n7_translit;

@end
