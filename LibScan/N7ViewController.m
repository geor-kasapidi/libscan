//
//  N7ViewController.m
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewController.h"

@implementation N7ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setupUI)]) {
        [self setupUI];
    }
    
    if ([self respondsToSelector:@selector(bindUI)]) {
        [self bindUI];
    }
    
    if ([self respondsToSelector:@selector(localizeUI)]) {
        [self localizeUI];
    }
}

@end
