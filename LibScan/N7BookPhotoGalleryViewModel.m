//
//  N7BookPhotoGalleryViewModel.m
//  LibScan
//
//  Created by Георгий Касапиди on 19.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7BookPhotoGalleryViewModel.h"
#import "N7BookModel.h"
#import "N7BooksFileSystemManager.h"

@interface N7BookPhotoGalleryViewModel ()

@property (strong, nonatomic) N7BookModel *bookModel;

@property (strong, nonatomic) NSMutableArray<NSUUID *> *bookPagesIDs;

@end

@implementation N7BookPhotoGalleryViewModel

- (instancetype)initWithBookModel:(N7BookModel *)bookModel {
    NSParameterAssert(bookModel != nil);
    
    self = [super init];
    
    if (self) {
        self.bookModel = bookModel;
    }
    
    return self;
}

- (void)activate {
    NSMutableArray *bookPagesIDs = [NSMutableArray array];
    
    [bookPagesIDs addObjectsFromArray:[[N7BooksFileSystemManager manager] pagesIDsForBook:self.bookModel]];
    
    self.bookPagesIDs = bookPagesIDs;
}

- (NSString *)bookName {
    return self.bookModel.name;
}

- (NSInteger)numberOfPhotos {
    return self.bookPagesIDs.count;
}

- (UIImage *)originalImageAtIndex:(NSInteger)index {
    NSUUID *pageID = self.bookPagesIDs[index];
    
    UIImage *originalImage = [[N7BooksFileSystemManager manager] pageOriginalImageWithID:pageID inBook:self.bookModel];
    
    return originalImage;
}

@end
