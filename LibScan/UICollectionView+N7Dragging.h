//
//  UICollectionView+N7Dragging.h
//  LibScan
//
//  Created by Георгий Касапиди on 18.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionView (N7Dragging)

- (void)n7_enableDragging;

@end
