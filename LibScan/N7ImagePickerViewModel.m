//
//  N7ImagePickerViewModel.m
//  LibScan
//
//  Created by Георгий Касапиди on 16.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>

#import "N7ImagePickerViewModel.h"

#import "UIImage+N7Helpers.h"

static const CGFloat N7ImagePickerPreviewImageWidth = 200;

static NSString * const UIApplicationVolumeDownButtonUpNotification = @"_UIApplicationVolumeDownButtonUpNotification";
static NSString * const UIApplicationVolumeUpButtonUpNotification = @"_UIApplicationVolumeUpButtonUpNotification";

@implementation N7ImagePickerViewModel

- (void)activate {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(takePhoto)
                                                 name:UIApplicationVolumeDownButtonUpNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(takePhoto)
                                                 name:UIApplicationVolumeUpButtonUpNotification
                                               object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Actions

- (void)takePhoto {
    [self.delegate viewModelTakePhoto:self];
}

- (void)processOriginalImage:(UIImage *)originalImage {    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        CGSize targetSize = CGSizeMake(N7ImagePickerPreviewImageWidth, N7ImagePickerPreviewImageWidth * originalImage.size.height / originalImage.size.width);
        
        UIImage *previewImage = [originalImage n7_previewWithSize:targetSize];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate viewModel:self didFinishOriginalImageProcessing:originalImage previewImage:previewImage];
        });
    });
}

@end
