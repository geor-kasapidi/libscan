//
//  N7BookPagesViewModel.h
//  LibScan
//
//  Created by Георгий Касапиди on 16.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewModel.h"

@class UIImage;
@class N7BookModel;

@class N7BookPagesViewModel;

@protocol N7BookPagesViewModelDelegate  <NSObject>

@required

- (void)viewModel:(N7BookPagesViewModel *)viewModel insertItemAtIndexPath:(NSIndexPath *)indexPath;
- (void)viewModel:(N7BookPagesViewModel *)viewModel deleteItemAtIndexPath:(NSIndexPath *)indexPath;

- (void)viewModel:(N7BookPagesViewModel *)viewModel setEmptyViewVisible:(BOOL)emptyViewVisible;

@end

@interface N7BookPagesViewModel : N7ViewModel

@property (weak, nonatomic) id<N7BookPagesViewModelDelegate> delegate; // view controller

@property (strong, nonatomic, readonly) N7BookModel *bookModel;

- (instancetype)initWithBookModel:(N7BookModel *)bookModel;

- (void)activate;

- (NSInteger)numberOfPages;
- (UIImage *)pagePreviewImageAtIndexPath:(NSIndexPath *)indexPath;
- (UIImage *)lastPagePreviewImage;

- (void)createNewPageWithPhoto:(UIImage *)photo preview:(UIImage *)preview;
- (void)deletePageAtIndexPath:(NSIndexPath *)indexPath;
- (void)movePageFromIndexPath:(NSIndexPath *)oldIndexPath toIndexPath:(NSIndexPath *)newIndexPath;

@end
