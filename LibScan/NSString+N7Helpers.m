//
//  NSString+N7Helpers.m
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "NSString+N7Helpers.h"

@implementation NSString (N7Helpers)

- (NSString *)n7_trim {
    return [self stringByTrimmingCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]];
}

- (NSString *)n7_alpanumWithJoiner:(NSString *)joiner {
    return [[self componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]] componentsJoinedByString:joiner ?: @""];
}

- (NSString *)n7_translit {
    return [self stringByApplyingTransform:NSStringTransformLatinToCyrillic reverse:YES];
}

@end
