//
//  N7BooksCollectionViewCell.h
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "N7ConfigurableObject.h"

@class N7BooksCollectionViewCell;

@protocol N7BooksCollectionViewCellDelegate <NSObject>

@required

- (void)bookCellRenameBook:(N7BooksCollectionViewCell *)bookCell;
- (void)bookCellDeleteBook:(N7BooksCollectionViewCell *)bookCell;

@end

@interface N7BooksCollectionViewCell : UICollectionViewCell <N7ConfigurableObject>

@property (weak, nonatomic) id<N7BooksCollectionViewCellDelegate> delegate;

// Custom menu actions

- (void)renameBook:(id)sender;
- (void)deleteBook:(id)sender;

@end
