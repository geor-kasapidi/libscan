//
//  N7BookPhotoGalleryViewController.m
//  LibScan
//
//  Created by Георгий Касапиди on 19.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7BookPhotoGalleryViewController.h"
#import "N7BookPhotoGalleryViewModel.h"
#import "N7BookPhotoGalleryImageViewController.h"

@interface N7BookPhotoGalleryViewController () <UIPageViewControllerDelegate, UIPageViewControllerDataSource, N7BookPhotoGalleryImageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@property (strong, nonatomic) N7BookPhotoGalleryViewModel *viewModel;

@property (assign, nonatomic) NSInteger startIndex;

@end

@implementation N7BookPhotoGalleryViewController

+ (instancetype)photoGalleryForBook:(N7BookModel *)bookModel withStartIndex:(NSInteger)startIndex {
    N7BookPhotoGalleryViewController *vc = [self new];
    
    vc.viewModel = [[N7BookPhotoGalleryViewModel alloc] initWithBookModel:bookModel];
    
    vc.startIndex = startIndex;
    
    return vc;
}

#pragma mark - Setup

- (void)setupUI {
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:nil action:nil];
    
    UIPageViewController *pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    [self addChildViewController:pageViewController];
    
    pageViewController.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:pageViewController.view];
    
    [pageViewController.view.topAnchor constraintEqualToAnchor:self.view.topAnchor];
    [pageViewController.view.rightAnchor constraintEqualToAnchor:self.view.rightAnchor];
    [pageViewController.view.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor];
    [pageViewController.view.leftAnchor constraintEqualToAnchor:self.view.leftAnchor];
    
    [pageViewController didMoveToParentViewController:self];
    
    self.pageViewController = pageViewController;
}

- (void)bindUI {
    [self.viewModel activate];
    
    self.navigationItem.leftBarButtonItem.target = self;
    self.navigationItem.leftBarButtonItem.action = @selector(closeGallery);
    
    self.navigationItem.rightBarButtonItem.target = self;
    self.navigationItem.rightBarButtonItem.action = @selector(closeGalleryAndOpenCamera);
    
    self.navigationItem.prompt = [self.viewModel bookName];
    
    self.pageViewController.delegate = self;
    self.pageViewController.dataSource = self;
    
    [self.pageViewController setViewControllers:@[[self imageViewControllerAtIndex:self.startIndex]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

- (void)localizeUI {
    self.navigationItem.leftBarButtonItem.title = NSLocalizedString(@"Закрыть", nil);
}

#pragma mark - Actions

- (void)closeGallery {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)closeGalleryAndOpenCamera {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate galleryViewControllerOpenCamera];
    }];
}

#pragma mark - Page view controller data source

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(N7BookPhotoGalleryImageViewController *)viewController {
    return [self imageViewControllerAtIndex:viewController.photoIndex - 1];
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(N7BookPhotoGalleryImageViewController *)viewController {
    return [self imageViewControllerAtIndex:viewController.photoIndex + 1];
}

#pragma mark - Image view controller delegate

- (void)imageViewControllerDidAppear:(N7BookPhotoGalleryImageViewController *)imageViewController {
    self.navigationItem.title = [NSString stringWithFormat:@"%@ %@ %@", @(imageViewController.photoIndex + 1), NSLocalizedString(@"из", nil), @([self.viewModel numberOfPhotos])];
}

#pragma mark - Private

- (N7BookPhotoGalleryImageViewController *)imageViewControllerAtIndex:(NSInteger)index {
    if (index >= 0 && index < [self.viewModel numberOfPhotos]) {
        UIImage *image = [self.viewModel originalImageAtIndex:index];
        
        N7BookPhotoGalleryImageViewController *imageViewController = [N7BookPhotoGalleryImageViewController imageViewControllerWithImage:image andIndex:index];
        
        imageViewController.delegate = self;
        
        return imageViewController;
    }
    
    return nil;
}

@end
