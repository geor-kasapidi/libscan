//
//  N7BookModel.h
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface N7BookModel : NSObject

@property (copy, nonatomic) NSUUID *bookID;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *user;

+ (instancetype)modelWithName:(NSString *)name user:(NSString *)user;
+ (instancetype)modelWithID:(NSUUID *)bookID name:(NSString *)name user:(NSString *)user;

@end
