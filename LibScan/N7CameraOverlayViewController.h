//
//  N7CameraOverlayViewController.h
//  LibScan
//
//  Created by Георгий Касапиди on 16.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewController.h"

@class N7CameraOverlayViewController;

@protocol N7CameraOverlayViewControllerDelegate <NSObject>

@required

- (void)cameraOverlayTakePhoto:(N7CameraOverlayViewController *)cameraOverlay;
- (void)cameraOverlayClosePicker:(N7CameraOverlayViewController *)cameraOverlay;

@end

@interface N7CameraOverlayViewController : N7ViewController

@property (weak, nonatomic) id<N7CameraOverlayViewControllerDelegate> delegate;

- (void)showPreviewImage:(UIImage *)previewImage;

@end
