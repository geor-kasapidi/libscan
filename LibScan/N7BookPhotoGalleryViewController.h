//
//  N7BookPhotoGalleryViewController.h
//  LibScan
//
//  Created by Георгий Касапиди on 19.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewController.h"

@class N7BookModel;

@class N7BookPhotoGalleryViewController;

@protocol N7BookPhotoGalleryViewControllerDelegate <NSObject>

@required

- (void)galleryViewControllerOpenCamera;

@end

@interface N7BookPhotoGalleryViewController : N7ViewController

@property (weak, nonatomic) id<N7BookPhotoGalleryViewControllerDelegate> delegate;

+ (instancetype)photoGalleryForBook:(N7BookModel *)bookModel withStartIndex:(NSInteger)startIndex;

@end
