//
//  N7BookPagesViewModel.m
//  LibScan
//
//  Created by Георгий Касапиди on 16.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7BookPagesViewModel.h"
#import "N7BookModel.h"
#import "N7BooksFileSystemManager.h"

@interface N7BookPagesViewModel ()

@property (strong, nonatomic) N7BookModel *bookModel;

@property (strong, nonatomic) NSMutableArray<NSUUID *> *bookPagesIDs;

@end

@implementation N7BookPagesViewModel

- (instancetype)initWithBookModel:(N7BookModel *)bookModel {
    NSParameterAssert(bookModel != nil);
    
    self = [super init];
    
    if (self) {
        self.bookModel = bookModel;
    }
    
    return self;
}

- (void)activate {
    NSMutableArray *bookPagesIDs = [NSMutableArray array];
    
    [bookPagesIDs addObjectsFromArray:[[N7BooksFileSystemManager manager] pagesIDsForBook:self.bookModel]];
    
    self.bookPagesIDs = bookPagesIDs;
    
    [self.delegate viewModel:self setEmptyViewVisible:[self numberOfPages] == 0];
}

- (NSString *)bookName {
    return self.bookModel.name;
}

- (NSString *)userName {
    return self.bookModel.user;
}

- (NSInteger)numberOfPages {
    return self.bookPagesIDs.count;
}

- (UIImage *)pagePreviewImageAtIndexPath:(NSIndexPath *)indexPath {
    NSParameterAssert(indexPath.length == 2);
    
    UIImage *previewImage = [self pagePreviewImageAtIndex:[indexPath indexAtPosition:1]];
    
    return previewImage;
}

- (UIImage *)lastPagePreviewImage {
    NSInteger numberOfPages = [self numberOfPages];
    
    if (numberOfPages == 0) {
        return nil;
    }
    
    UIImage *previewImage = [self pagePreviewImageAtIndex:numberOfPages - 1];
    
    return previewImage;
}

- (void)createNewPageWithPhoto:(UIImage *)photo preview:(UIImage *)preview {
    [[N7BooksFileSystemManager manager] createBookPageWithOriginalImage:photo andPreviewImage:preview inBook:self.bookModel withCompletionHandler:^(NSUUID *pageID) {
        if (pageID) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.bookPagesIDs addObject:pageID];
                
                NSIndexPath *indexPath = [[NSIndexPath indexPathWithIndex:0] indexPathByAddingIndex:[self.bookPagesIDs indexOfObject:pageID]];
                
                [self.delegate viewModel:self insertItemAtIndexPath:indexPath];
                [self.delegate viewModel:self setEmptyViewVisible:[self numberOfPages] == 0];
            });
        }
    }];
}

- (void)deletePageAtIndexPath:(NSIndexPath *)indexPath {
    NSParameterAssert(indexPath.length == 2);
    
    NSUUID *pageID = self.bookPagesIDs[[indexPath indexAtPosition:1]];
    
    if ([[N7BooksFileSystemManager manager] deletePageWithID:pageID inBook:self.bookModel]) {
        [self.bookPagesIDs removeObject:pageID];
        
        [self.delegate viewModel:self deleteItemAtIndexPath:indexPath];
        [self.delegate viewModel:self setEmptyViewVisible:[self numberOfPages] == 0];
    }
}

- (void)movePageFromIndexPath:(NSIndexPath *)oldIndexPath toIndexPath:(NSIndexPath *)newIndexPath {
    NSParameterAssert(oldIndexPath.length == 2);
    NSParameterAssert(newIndexPath.length == 2);
    
    NSUInteger oldIndex = [oldIndexPath indexAtPosition:1];
    NSUInteger newIndex = [newIndexPath indexAtPosition:1];
    
    if (oldIndex == newIndex) {
        return;
    }
    
    NSUUID *pageID = self.bookPagesIDs[oldIndex];
    
    if ([[N7BooksFileSystemManager manager] moveFromCurrentIndexPageWithID:pageID toPosition:newIndex inBook:self.bookModel]) {
        [self.bookPagesIDs removeObjectAtIndex:oldIndex];
        [self.bookPagesIDs insertObject:pageID atIndex:newIndex];
    }
}

#pragma mark - Private

- (UIImage *)pagePreviewImageAtIndex:(NSInteger)index {
    NSUUID *pageID = self.bookPagesIDs[index];
    
    UIImage *previewImage = [[N7BooksFileSystemManager manager] pagePreviewImageWithID:pageID inBook:self.bookModel];
    
    return previewImage;
}

@end
