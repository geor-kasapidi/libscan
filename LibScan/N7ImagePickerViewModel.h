//
//  N7ImagePickerViewModel.h
//  LibScan
//
//  Created by Георгий Касапиди on 16.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewModel.h"

@class UIImage;
@class N7ImagePickerViewModel;

@protocol N7ImagePickerViewModelDelegate <NSObject>

@required

- (void)viewModelTakePhoto:(N7ImagePickerViewModel *)viewModel;

- (void)viewModel:(N7ImagePickerViewModel *)viewModel didFinishOriginalImageProcessing:(UIImage *)originalImage previewImage:(UIImage *)previewImage;

@end

@interface N7ImagePickerViewModel : N7ViewModel

@property (weak, nonatomic) id<N7ImagePickerViewModelDelegate> delegate;

- (void)activate;

- (void)processOriginalImage:(UIImage *)originalImage;

@end
