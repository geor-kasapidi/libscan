//
//  N7BookPagePreviewCollectionViewCell.h
//  LibScan
//
//  Created by Георгий Касапиди on 18.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <UIKit/UIKit.h>

@class N7BookPagePreviewCollectionViewCell;

@protocol N7BookPagePreviewCollectionViewCellDelegate <NSObject>

@required

- (void)bookPagePreviewCellDeletePage:(N7BookPagePreviewCollectionViewCell *)bookPagePreviewCell;

@end

@interface N7BookPagePreviewCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) id<N7BookPagePreviewCollectionViewCellDelegate> delegate;

- (void)showPreviewImage:(UIImage *)previewImage withNumber:(NSInteger)number;

@end
