//
//  N7CameraOverlayViewController.m
//  LibScan
//
//  Created by Георгий Касапиди on 16.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7CameraOverlayViewController.h"
#import "N7Button.h"

@interface N7CameraOverlayViewController ()

@property (weak, nonatomic) IBOutlet UIView *takePhotoButtonContainerView;
@property (weak, nonatomic) IBOutlet N7Button *takePhotoButton;
@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end

@implementation N7CameraOverlayViewController

#pragma mark - Setup

- (void)setupUI {
    self.view.backgroundColor = [UIColor clearColor];
}

#pragma mark - Orientation

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape; // not working :(
}

#pragma mark - Layout

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.takePhotoButtonContainerView.layer.cornerRadius = CGRectGetHeight(self.takePhotoButtonContainerView.frame) / 2;
        self.takePhotoButton.layer.cornerRadius = CGRectGetHeight(self.takePhotoButton.frame) / 2;
        
        self.takePhotoButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
        self.takePhotoButton.layer.borderWidth = 1;
    });
}

#pragma mark - Public

- (void)showPreviewImage:(UIImage *)previewImage {
    self.previewImageView.image = previewImage;
}

#pragma mark - Actions

- (IBAction)closeButtonTapped:(id)sender {
    [self.delegate cameraOverlayClosePicker:self];
}

- (IBAction)takePhotoButtonTapped:(id)sender {
    [self.delegate cameraOverlayTakePhoto:self];
}

@end
