//
//  N7PersistentUUIDArray.m
//  LibScan
//
//  Created by Георгий Касапиди on 18.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7PersistentUUIDArray.h"

@interface N7PersistentUUIDArray ()

@property (copy, nonatomic) NSString *filePath;

@property (strong, nonatomic) NSMutableArray<NSString *> *backArray;

@end

@implementation N7PersistentUUIDArray

#pragma mark - Initialization

- (instancetype)initWithFilePath:(NSString *)filePath {
    self = [super init];
    
    if (self) {
        self.filePath = filePath;
        
        NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
        NSArray<NSString *> *contentArray = [(content ?: @"") componentsSeparatedByString:@"\n"];
        
        NSMutableArray *backArray = [NSMutableArray array];
        
        [contentArray enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (obj.length > 0) {
                [backArray addObject:obj];
            }
        }];
        
        self.backArray = backArray;
    }
    
    return self;
}

#pragma mark - Public

- (NSArray<NSUUID *> *)uuIDs {
    NSMutableArray *array = [NSMutableArray array];
    
    [self.backArray enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.length > 0) {
            NSUUID *uuID = [[NSUUID alloc] initWithUUIDString:obj];
            
            [array addObject:uuID];
        }
    }];
    
    return [array copy];
}

- (BOOL)addID:(NSUUID *)uuID {
    NSParameterAssert(uuID != nil);
    
    [self.backArray addObject:[uuID UUIDString]];
    
    NSString *content = [self.backArray componentsJoinedByString:@"\n"];
    
    return [content writeToFile:self.filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

- (BOOL)deleteUUID:(NSUUID *)uuID {
    NSParameterAssert(uuID != nil);
    
    NSUInteger indexOfUUID = [self.backArray indexOfObject:[uuID UUIDString]];
    
    if (indexOfUUID == NSNotFound) {
        return NO;
    }
    
    [self.backArray removeObjectAtIndex:indexOfUUID];
    
    NSString *content = [self.backArray componentsJoinedByString:@"\n"];
    
    return [content writeToFile:self.filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

- (BOOL)moveUUID:(NSUUID *)uuID toIndex:(NSUInteger)index {
    NSParameterAssert(uuID != nil);
    NSParameterAssert(index != NSNotFound);
    
    NSUInteger indexOfUUID = [self.backArray indexOfObject:[uuID UUIDString]];
    
    if (indexOfUUID == NSNotFound) {
        return NO;
    }
    
    [self.backArray removeObjectAtIndex:indexOfUUID];
    [self.backArray insertObject:[uuID UUIDString] atIndex:index];
    
    NSString *content = [self.backArray componentsJoinedByString:@"\n"];
    
    return [content writeToFile:self.filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

@end
