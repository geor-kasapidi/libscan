//
//  N7ConfigurableObject.h
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol N7ConfigurableObject <NSObject>

@required

- (void)configureWithModel:(id)model;

@end
