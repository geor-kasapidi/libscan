//
//  N7BookArchiveWebResponse.m
//  LibScan
//
//  Created by Георгий Касапиди on 18.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

@import GCDWebServer;

#import "N7BookArchiveWebResponse.h"
#import "N7BooksFileSystemManager.h"

@implementation N7BookArchiveWebResponse

- (NSString *)method {
    return @"GET";
}

- (NSString *)path {
    return @"/book";
}

- (GCDWebServerResponse *)serverResponseForRequest:(GCDWebServerRequest *)request {
    NSString *bookIDString = request.query[@"id"];
    
    if (bookIDString.length == 0) {
        return [GCDWebServerResponse response];
    }
    
    NSUUID *bookID = [[NSUUID alloc] initWithUUIDString:bookIDString];
    
    if (!bookID) {
        return [GCDWebServerResponse response];
    }
    
    N7BookModel *book = [[N7BooksFileSystemManager manager] findBookByID:bookID];
    
    if (!book) {
        return [GCDWebServerResponse response];
    }
    
    NSString *bookZipFilePath = [[N7BooksFileSystemManager manager] zipBookContent:book];
    
    if (bookZipFilePath.length == 0) {
        return [GCDWebServerResponse response];
    }
    
    return [GCDWebServerFileResponse responseWithFile:bookZipFilePath isAttachment:YES];
}

@end
