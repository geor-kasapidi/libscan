//
//  N7BooksCollectionViewCell.m
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7BooksCollectionViewCell.h"
#import "N7BookModel.h"

@interface N7BooksCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *bookNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;

@end

@implementation N7BooksCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentView.backgroundColor = [UIColor whiteColor];
}

#pragma mark - Custom menu actions

- (void)renameBook:(id)sender {
    [self.delegate bookCellRenameBook:self];
}

- (void)deleteBook:(id)sender {
    [self.delegate bookCellDeleteBook:self];
}

#pragma mark - Configurable object

- (void)configureWithModel:(id)model {
    if ([model isKindOfClass:[N7BookModel class]]) {
        N7BookModel *book = (N7BookModel *)model;
        
        self.bookNameLabel.text = book.name;
        self.userLabel.text = book.user;
    }
}

@end
