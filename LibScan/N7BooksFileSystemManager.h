//
//  N7BooksFileSystemManager.h
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIImage;
@class N7BookModel;

@interface N7BooksFileSystemManager : NSObject

+ (instancetype)manager;

- (BOOL)createOrUpdateBook:(N7BookModel *)book;
- (BOOL)deleteBook:(N7BookModel *)book;

- (NSArray<N7BookModel *> *)booksArray;
- (N7BookModel *)findBookByID:(NSUUID *)bookID;

- (void)createBookPageWithOriginalImage:(UIImage *)originalImage andPreviewImage:(UIImage *)previewImage inBook:(N7BookModel *)book withCompletionHandler:(void (^)(NSUUID *pageID))completionHandler;
- (BOOL)deletePageWithID:(NSUUID *)pageID inBook:(N7BookModel *)book;
- (BOOL)moveFromCurrentIndexPageWithID:(NSUUID *)pageID toPosition:(NSUInteger)newIndex inBook:(N7BookModel *)book;

- (NSArray<NSUUID *> *)pagesIDsForBook:(N7BookModel *)book;

- (UIImage *)pagePreviewImageWithID:(NSUUID *)pageID inBook:(N7BookModel *)book;
- (UIImage *)pageOriginalImageWithID:(NSUUID *)pageID inBook:(N7BookModel *)book;

- (NSString *)zipBookContent:(N7BookModel *)book;

- (void)clearTempDir;

@end
