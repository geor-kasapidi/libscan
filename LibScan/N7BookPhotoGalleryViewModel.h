//
//  N7BookPhotoGalleryViewModel.h
//  LibScan
//
//  Created by Георгий Касапиди on 19.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewModel.h"

@class UIImage;
@class N7BookModel;

@interface N7BookPhotoGalleryViewModel : N7ViewModel

- (instancetype)initWithBookModel:(N7BookModel *)bookModel;

- (void)activate;

- (NSString *)bookName;

- (NSInteger)numberOfPhotos;

- (UIImage *)originalImageAtIndex:(NSInteger)index;

@end
