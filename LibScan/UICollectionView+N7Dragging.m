//
//  UICollectionView+N7Dragging.m
//  LibScan
//
//  Created by Георгий Касапиди on 18.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "UICollectionView+N7Dragging.h"

@implementation UICollectionView (N7Dragging)

- (void)n7_enableDragging {
    [self addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(__handleDragging:)]];
}

#pragma mark - Private

- (void)__handleDragging:(UILongPressGestureRecognizer *)recognizer {
    if (recognizer.view != self) {
        return;
    }
    
    CGPoint location = [recognizer locationInView:self];
    
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan: {
            NSIndexPath *indexPath = [self indexPathForItemAtPoint:location];
            
            if (indexPath.length == 2) {
                [self beginInteractiveMovementForItemAtIndexPath:indexPath];
            }
            
            break;
        }
        case UIGestureRecognizerStateChanged: {
            [self updateInteractiveMovementTargetPosition:location];
            
            break;
        }
        case UIGestureRecognizerStateEnded: {
            [self endInteractiveMovement];
            
            break;
        }
        default: {
            [self cancelInteractiveMovement];
            
            break;
        }
    }
}

@end
