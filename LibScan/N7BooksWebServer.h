//
//  N7BooksWebServer.h
//  LibScan
//
//  Created by Георгий Касапиди on 18.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "N7WebResponse.h"

@interface N7BooksWebServer : NSObject

+ (instancetype)defaultServer;

- (NSString *)serverAddress;

- (void)registerResponses:(NSArray<NSObject<N7WebResponse> *> *)responses;

- (void)start;
- (void)stop;

@end
