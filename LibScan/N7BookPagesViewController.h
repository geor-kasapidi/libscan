//
//  N7BookPagesViewController.h
//  LibScan
//
//  Created by Георгий Касапиди on 16.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewController.h"

@class N7BookModel;

@interface N7BookPagesViewController : N7ViewController

+ (instancetype)bookPagesViewControllerWithBookModel:(N7BookModel *)bookModel;

@end
