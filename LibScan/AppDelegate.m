//
//  AppDelegate.m
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "AppDelegate.h"
#import "N7BooksWebServer.h"
#import "N7BooksFileSystemManager.h"

#import "N7BookListWebResponse.h"
#import "N7BookArchiveWebResponse.h"

#import "N7NavigationController.h"
#import "N7BooksListViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self startServer];
    
    N7BooksListViewController *booksLitVC = [N7BooksListViewController new];
    
    self.window = [UIWindow new];
    
    self.window.rootViewController = [[N7NavigationController alloc] initWithRootViewController:booksLitVC];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [[N7BooksWebServer defaultServer] stop];
    
    [[N7BooksFileSystemManager manager] clearTempDir];
}

#pragma mark - Private

- (void)startServer {
    N7BookListWebResponse *booksResponse = [N7BookListWebResponse new];
    N7BookArchiveWebResponse *bookArchiveResponse = [N7BookArchiveWebResponse new];
    
    [[N7BooksWebServer defaultServer] registerResponses:@[booksResponse, bookArchiveResponse]];
    [[N7BooksWebServer defaultServer] start];
}

@end
