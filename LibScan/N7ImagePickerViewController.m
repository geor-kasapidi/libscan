//
//  N7ImagePickerViewController.m
//  LibScan
//
//  Created by Георгий Касапиди on 16.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ImagePickerViewController.h"
#import "N7ViewPreferredActions.h"
#import "N7CameraOverlayViewController.h"
#import "N7ImagePickerViewModel.h"

static const CGFloat N7ImagePickerCameraOverlayViewWidth = 200;

@interface N7ImagePickerViewController () <N7ViewPreferredActions, UINavigationControllerDelegate, UIImagePickerControllerDelegate, N7CameraOverlayViewControllerDelegate, N7ImagePickerViewModelDelegate>

@property (strong, nonatomic) UIImage *previewImage;

@property (strong, nonatomic) N7CameraOverlayViewController *cameraOverlayViewController;

@property (strong, nonatomic) N7ImagePickerViewModel *viewModel;

@end

@implementation N7ImagePickerViewController

- (instancetype)initWithPreviewImage:(UIImage *)previewImage {
    self = [super init];
    
    if (self) {
        self.previewImage = previewImage;
    }
    
    return self;
}

#pragma mark - Setup

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self bindUI];
}

- (void)setupUI {
    self.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.showsCameraControls = NO;
    self.allowsEditing = NO;
}

- (void)bindUI {
    self.viewModel = [N7ImagePickerViewModel new];
    
    self.viewModel.delegate = self;
    
    self.delegate = self;
    
    [self.viewModel activate];
}

#pragma mark - Layout

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);
    CGFloat viewHeight = CGRectGetHeight(self.view.frame);
    
    self.cameraOverlayView.frame = CGRectMake(viewWidth - N7ImagePickerCameraOverlayViewWidth, 0, N7ImagePickerCameraOverlayViewWidth, viewHeight);
}

#pragma mark - Navigation controller delegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (navigationController != self || self.cameraOverlayViewController != nil || [viewController isKindOfClass:[N7CameraOverlayViewController class]]) {
        return;
    }
    
    self.cameraOverlayViewController = [N7CameraOverlayViewController new];
    
    self.cameraOverlayViewController.delegate = self;
    
    self.cameraOverlayView = self.cameraOverlayViewController.view;
    
    [self.cameraOverlayViewController showPreviewImage:self.previewImage];
    
    self.previewImage = nil;
}

#pragma mark - Image picker controller delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    
    [self.viewModel processOriginalImage:originalImage];
}

#pragma mark - Camera overlay delegate

- (void)cameraOverlayTakePhoto:(N7CameraOverlayViewController *)cameraOverlay {
    [self takePicture];
}

- (void)cameraOverlayClosePicker:(N7CameraOverlayViewController *)cameraOverlay {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.pickerDelegate imagePickerDismissed];
    }];
}

#pragma mark - View model delegate

- (void)viewModelTakePhoto:(N7ImagePickerViewModel *)viewModel {
    [self takePicture];
}

- (void)viewModel:(N7ImagePickerViewModel *)viewModel didFinishOriginalImageProcessing:(UIImage *)originalImage previewImage:(UIImage *)previewImage {
    [self.cameraOverlayViewController showPreviewImage:previewImage];
    
    [self.pickerDelegate imagePicker:self savePhoto:originalImage preview:previewImage];
}

@end
