//
//  N7ViewController.h
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "N7ViewPreferredActions.h"

@interface N7ViewController : UIViewController <N7ViewPreferredActions>

@end
