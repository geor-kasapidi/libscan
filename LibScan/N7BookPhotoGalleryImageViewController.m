//
//  N7BookPhotoGalleryImageViewController.m
//  LibScan
//
//  Created by Георгий Касапиди on 19.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7BookPhotoGalleryImageViewController.h"

@interface N7BookPhotoGalleryImageViewController () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) UIImage *image;
@property (assign, nonatomic) NSInteger photoIndex;

@end

@implementation N7BookPhotoGalleryImageViewController

+ (instancetype)imageViewControllerWithImage:(UIImage *)image andIndex:(NSInteger)index {
    N7BookPhotoGalleryImageViewController *vc = [self new];
    
    vc.image = image;
    vc.photoIndex = index;
    
    return vc;
}

#pragma mark - Life cycle

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.delegate imageViewControllerDidAppear:self];
}

#pragma mark - Setup

- (void)setupUI {
    self.scrollView.minimumZoomScale = 1;
    self.scrollView.maximumZoomScale = 3;
}

- (void)bindUI {
    self.scrollView.delegate = self;
    
    self.imageView.image = self.image;
    self.image = nil;
}

#pragma mark - Scroll view delegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

@end
