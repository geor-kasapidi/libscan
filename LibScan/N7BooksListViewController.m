//
//  N7BooksListViewController.m
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7BooksListViewController.h"
#import "N7BooksListViewModel.h"
#import "N7BookModel.h"
#import "N7BooksCollectionViewCell.h"
#import "N7BookPagesViewController.h"
#import "N7NavigationController.h"

#import "UICollectionView+N7Helpers.h"
#import "NSString+N7Helpers.h"

static const CGFloat N7BooksCollectionItemSpace = 16;
static const CGFloat N7BooksCollectionMaxItemsInRowCount = 5;

@interface N7BooksListViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, N7BooksListViewModelDelegate, N7BooksCollectionViewCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *booksCollectionView;
@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UILabel *emptyTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *createBookButton;

@property (strong, nonatomic) N7BooksListViewModel *viewModel;

@end

@implementation N7BooksListViewController

- (void)setupUI {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:nil action:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:nil action:nil];
    
    self.booksCollectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.booksCollectionView.collectionViewLayout;
    
    flowLayout.minimumLineSpacing = N7BooksCollectionItemSpace;
    flowLayout.minimumInteritemSpacing = N7BooksCollectionItemSpace;
    flowLayout.headerReferenceSize = CGSizeZero;
    flowLayout.footerReferenceSize = CGSizeZero;
    flowLayout.sectionInset = UIEdgeInsetsMake(N7BooksCollectionItemSpace, N7BooksCollectionItemSpace, N7BooksCollectionItemSpace, N7BooksCollectionItemSpace);
    
    [self.booksCollectionView n7_registerNIBForCellClass:[N7BooksCollectionViewCell class]];
}

- (void)bindUI {
    self.viewModel = [N7BooksListViewModel new];
    
    self.viewModel.delegate = self;
    
    self.booksCollectionView.delegate = self;
    self.booksCollectionView.dataSource = self;
    
    self.navigationItem.leftBarButtonItem.target = self;
    self.navigationItem.leftBarButtonItem.action = @selector(showServerInfo);
    
    self.navigationItem.rightBarButtonItem.target = self;
    self.navigationItem.rightBarButtonItem.action = @selector(createBook);
    [self.createBookButton addTarget:self action:@selector(createBook) forControlEvents:UIControlEventTouchUpInside];
    
    UIMenuItem *renameBookMenuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Редактировать", nil) action:@selector(renameBook:)];
    UIMenuItem *deleteBookMenuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Удалить", nil) action:@selector(deleteBook:)];
    
    [[UIMenuController sharedMenuController] setMenuItems:@[renameBookMenuItem, deleteBookMenuItem]];
    
    [self.viewModel activate];
}

- (void)localizeUI {
    self.title = NSLocalizedString(@"Книги", nil);
    self.emptyTitleLabel.text = NSLocalizedString(@"Список пуст", nil);
    [self.createBookButton setTitle:NSLocalizedString(@"Создать книгу", nil) forState:UIControlStateNormal];
}

#pragma mark - Actions

- (void)showServerInfo {
    [self showServerInfoDialogWithServerAddress:[self.viewModel serverAddress]];
}

- (void)createBook {
    [self showBookEditingDialogForBook:nil withCompletionHandler:^(N7BookModel *book) {
        [self.viewModel createOrUpdateBook:book];
    }];
}

#pragma mark - Collection view data source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.viewModel numberOfBooks];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    N7BooksCollectionViewCell *cell = [collectionView n7_dequeueReusableCellWithClass:[N7BooksCollectionViewCell class] atIndexPath:indexPath];
    
    cell.delegate = self;
    
    N7BookModel *book = [self.viewModel bookAtIndexPath:indexPath];
    
    [cell configureWithModel:book];
    
    return cell;
}

#pragma mark - Collection view flow layout delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellWidth = (CGRectGetWidth(collectionView.bounds) - N7BooksCollectionItemSpace * (N7BooksCollectionMaxItemsInRowCount + 1)) / N7BooksCollectionMaxItemsInRowCount;
    
    CGSize cellSize = CGSizeMake(cellWidth, cellWidth * 4 / 3);
    
    return cellSize;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(nullable id)sender {
    return action == @selector(renameBook:) || action == @selector(deleteBook:);
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(nullable id)sender {
    // no implementation
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    N7BookModel *book = [self.viewModel bookAtIndexPath:indexPath];
    
    N7BookPagesViewController *bookPagesViewController = [N7BookPagesViewController bookPagesViewControllerWithBookModel:book];
    
    [self presentViewController:[[N7NavigationController alloc] initWithRootViewController:bookPagesViewController] animated:YES completion:nil];
}

#pragma mark - View model delegate

- (void)viewModel:(N7BooksListViewModel *)viewModel insertItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.length == 2) {
        [self.booksCollectionView insertItemsAtIndexPaths:@[indexPath]];
    }
}

- (void)viewModel:(N7BooksListViewModel *)viewModel deleteItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.length == 2) {
        [self.booksCollectionView deleteItemsAtIndexPaths:@[indexPath]];
    }
}

- (void)viewModel:(N7BooksListViewModel *)viewModel configureItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.length == 2) {
        UICollectionViewCell *cell = [self.booksCollectionView cellForItemAtIndexPath:indexPath];
        
        if ([cell isKindOfClass:[N7BooksCollectionViewCell class]]) {
            N7BookModel *book = [viewModel bookAtIndexPath:indexPath];
            
            [(N7BooksCollectionViewCell *)cell configureWithModel:book];
        }
    }
}

- (void)viewModel:(N7BooksListViewModel *)viewModel setEmptyViewVisible:(BOOL)emptyViewVisible {
    if (emptyViewVisible) {
        [self.emptyView.superview bringSubviewToFront:self.emptyView];
    } else {
        [self.emptyView.superview sendSubviewToBack:self.emptyView];
    }
}

#pragma mark - Book cell delegate

- (void)bookCellRenameBook:(N7BooksCollectionViewCell *)bookCell {
    NSIndexPath *indexPath = [self.booksCollectionView indexPathForCell:bookCell];
    
    if (indexPath.length == 2) {
        N7BookModel *book = [self.viewModel bookAtIndexPath:indexPath];
        
        [self showBookEditingDialogForBook:book withCompletionHandler:^(N7BookModel *book) {
            [self.viewModel createOrUpdateBook:book];
        }];
    }
}

- (void)bookCellDeleteBook:(N7BooksCollectionViewCell *)bookCell {
    NSIndexPath *indexPath = [self.booksCollectionView indexPathForCell:bookCell];
    
    if (indexPath.length == 2) {
        N7BookModel *book = [self.viewModel bookAtIndexPath:indexPath];
        
        [self showDeleteConfirmationDialogForBook:book withCompletionHandler:^{
            [self.viewModel deleteBook:book];
        }];
    }
}

#pragma mark - Dialogs

- (void)showServerInfoDialogWithServerAddress:(NSString *)serverAddress {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:serverAddress message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ОК", nil) style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showBookEditingDialogForBook:(N7BookModel *)book withCompletionHandler:(void (^)(N7BookModel *book))completionHandler {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Книга", nil) message:NSLocalizedString(@"", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = NSLocalizedString(@"Название", nil);
        textField.text = book.name;
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = NSLocalizedString(@"Пользователь", nil);
        textField.text = book.user;
    }];
    
    __weak typeof(alert) weakAlert = alert;
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ОК", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        __strong typeof(weakAlert) strongAlert = weakAlert;
        
        NSString *bookName = [strongAlert.textFields[0].text n7_trim];
        NSString *user = [strongAlert.textFields[1].text n7_trim];
        
        if (bookName.length > 0 && user.length > 0 && completionHandler) {
            if (book) {
                book.name = bookName;
                book.user = user;
                
                completionHandler(book);
            } else {
                N7BookModel *newBook = [N7BookModel modelWithName:bookName user:user];
                
                completionHandler(newBook);
            }
        }
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Отмена", nil) style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showDeleteConfirmationDialogForBook:(N7BookModel *)book withCompletionHandler:(void (^)(void))completionHandler {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Внимание", nil) message:[NSString stringWithFormat:NSLocalizedString(@"Вы уверены, что хотите удалить книгу \"%@\"", nil), book.name] preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ОК", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        if (completionHandler) {
            completionHandler();
        }
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Отмена", nil) style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
