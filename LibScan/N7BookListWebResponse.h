//
//  N7BookListWebResponse.h
//  LibScan
//
//  Created by Георгий Касапиди on 18.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "N7WebResponse.h"

@interface N7BookListWebResponse : NSObject <N7WebResponse>

@end
