//
//  N7BookPagesViewController.m
//  LibScan
//
//  Created by Георгий Касапиди on 16.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7BookPagesViewController.h"
#import "N7BookPagesViewModel.h"
#import "N7BookModel.h"
#import "N7ImagePickerViewController.h"
#import "N7BookPhotoGalleryViewController.h"
#import "N7NavigationController.h"
#import "N7BookPagePreviewCollectionViewCell.h"

#import "UICollectionView+N7Helpers.h"
#import "UICollectionView+N7Dragging.h"

static const CGFloat N7PhotosCollectionItemSpace = 16;
static const CGFloat N7PhotosCollectionMaxItemsInRowCount = 3;

@interface N7BookPagesViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, N7ImagePickerViewControllerDelegate, N7BookPhotoGalleryViewControllerDelegate, N7BookPagesViewModelDelegate, N7BookPagePreviewCollectionViewCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *photosCollectionView;

@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UILabel *emptyTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *addPhotoButton;

@property (strong, nonatomic) N7BookPagesViewModel *viewModel;

@end

@implementation N7BookPagesViewController

+ (instancetype)bookPagesViewControllerWithBookModel:(N7BookModel *)bookModel {
    N7BookPagesViewController *vc = [self new];
    
    vc.viewModel = [[N7BookPagesViewModel alloc] initWithBookModel:bookModel];
    vc.viewModel.delegate = vc;
    
    return vc;
}

#pragma mark - Setup

- (void)setupUI {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:nil style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:nil action:nil];
    
    self.photosCollectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.photosCollectionView.collectionViewLayout;
    
    flowLayout.minimumLineSpacing = N7PhotosCollectionItemSpace;
    flowLayout.minimumInteritemSpacing = N7PhotosCollectionItemSpace;
    flowLayout.headerReferenceSize = CGSizeZero;
    flowLayout.footerReferenceSize = CGSizeZero;
    flowLayout.sectionInset = UIEdgeInsetsMake(N7PhotosCollectionItemSpace, N7PhotosCollectionItemSpace, N7PhotosCollectionItemSpace, N7PhotosCollectionItemSpace);
    
    [self.photosCollectionView n7_registerNIBForCellClass:[N7BookPagePreviewCollectionViewCell class]];
    
    [self.photosCollectionView n7_enableDragging];
}

- (void)bindUI {
    self.navigationItem.title = self.viewModel.bookModel.name;
    self.navigationItem.prompt = self.viewModel.bookModel.user;
    
    self.navigationItem.leftBarButtonItem.target = self;
    self.navigationItem.leftBarButtonItem.action = @selector(closeBookPagesController);
    
    self.navigationItem.rightBarButtonItem.target = self;
    self.navigationItem.rightBarButtonItem.action = @selector(openImagePickerController);
    
    [self.addPhotoButton addTarget:self action:@selector(openImagePickerController) forControlEvents:UIControlEventTouchUpInside];
    
    self.photosCollectionView.delegate = self;
    self.photosCollectionView.dataSource = self;
    
    [self.viewModel activate];
}

- (void)localizeUI {
    self.navigationItem.leftBarButtonItem.title = NSLocalizedString(@"Закрыть", nil);
    self.emptyTitleLabel.text = NSLocalizedString(@"Список пуст", nil);
    [self.addPhotoButton setTitle:NSLocalizedString(@"Сделать фото", nil) forState:UIControlStateNormal];
}

#pragma mark - Actions

- (void)closeBookPagesController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)openImagePickerController {
    UIImage *previewImage = [self.viewModel lastPagePreviewImage];
    
    N7ImagePickerViewController *imagePickerController = [[N7ImagePickerViewController alloc] initWithPreviewImage:previewImage];
    
    imagePickerController.pickerDelegate = self;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

#pragma mark - Collection view data source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.viewModel numberOfPages];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    N7BookPagePreviewCollectionViewCell *cell = [collectionView n7_dequeueReusableCellWithClass:[N7BookPagePreviewCollectionViewCell class] atIndexPath:indexPath];
    
    cell.delegate = self;
    
    UIImage *previewImage = [self.viewModel pagePreviewImageAtIndexPath:indexPath];
    
    [cell showPreviewImage:previewImage withNumber:indexPath.item + 1];
    
    return cell;
}

#pragma mark - Collection view delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellWidth = (CGRectGetWidth(collectionView.bounds) - N7PhotosCollectionItemSpace * (N7PhotosCollectionMaxItemsInRowCount + 1)) / N7PhotosCollectionMaxItemsInRowCount;
    
    CGSize cellSize = CGSizeMake(cellWidth, cellWidth * 3 / 4);
    
    return cellSize;
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    [self.viewModel movePageFromIndexPath:sourceIndexPath toIndexPath:destinationIndexPath];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)([CATransaction animationDuration] * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.photosCollectionView reloadData];
    });
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    N7BookPhotoGalleryViewController *photoGallery = [N7BookPhotoGalleryViewController photoGalleryForBook:self.viewModel.bookModel withStartIndex:indexPath.item];
    
    photoGallery.delegate = self;
    
    [self presentViewController:[[N7NavigationController alloc] initWithRootViewController:photoGallery] animated:YES completion:nil];
}

#pragma mark - Image picker delegate

- (void)imagePicker:(N7ImagePickerViewController *)imagePicker savePhoto:(UIImage *)image preview:(UIImage *)preview {
    [self.viewModel createNewPageWithPhoto:image preview:preview];
}

- (void)imagePickerDismissed {
    NSInteger numberOfPages = [self.viewModel numberOfPages];
    
    if (numberOfPages == 0) {
        return;
    }
    
    N7BookPhotoGalleryViewController *photoGallery = [N7BookPhotoGalleryViewController photoGalleryForBook:self.viewModel.bookModel withStartIndex:numberOfPages - 1];
    
    photoGallery.delegate = self;
    
    [self presentViewController:[[N7NavigationController alloc] initWithRootViewController:photoGallery] animated:YES completion:nil];
}

#pragma mark - Gallery delegate

- (void)galleryViewControllerOpenCamera {
    [self openImagePickerController];
}

#pragma mark - View model delegate

- (void)viewModel:(N7BookPagesViewModel *)viewModel insertItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.length == 2) {
        [self.photosCollectionView insertItemsAtIndexPaths:@[indexPath]];
    }
}

- (void)viewModel:(N7BookPagesViewModel *)viewModel deleteItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.length == 2) {
        [self.photosCollectionView performBatchUpdates:^{
            [self.photosCollectionView deleteItemsAtIndexPaths:@[indexPath]];
        } completion:^(BOOL finished) {
            if (finished) {
                [self.photosCollectionView reloadData];
            }
        }];
    }
}

- (void)viewModel:(N7BookPagesViewModel *)viewModel setEmptyViewVisible:(BOOL)emptyViewVisible {
    if (emptyViewVisible) {
        [self.emptyView.superview bringSubviewToFront:self.emptyView];
    } else {
        [self.emptyView.superview sendSubviewToBack:self.emptyView];
    }
}

#pragma mark - Page preview cell delegate

- (void)bookPagePreviewCellDeletePage:(N7BookPagePreviewCollectionViewCell *)bookPagePreviewCell {
    NSIndexPath *indexPath = [self.photosCollectionView indexPathForCell:bookPagePreviewCell];
    
    if (indexPath.length == 2) {
        [self.viewModel deletePageAtIndexPath:indexPath];
    }
}

@end
