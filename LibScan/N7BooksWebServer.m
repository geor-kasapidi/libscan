//
//  N7BooksWebServer.m
//  LibScan
//
//  Created by Георгий Касапиди on 18.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

@import GCDWebServer;

#import "N7BooksWebServer.h"
#import "N7WebResponse.h"

#import "N7BookListWebResponse.h"

@interface N7BooksWebServer ()

@property (strong, nonatomic) GCDWebServer *webServer;

@end

@implementation N7BooksWebServer

+ (instancetype)defaultServer {
    static N7BooksWebServer *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [N7BooksWebServer new];
    });
    
    return instance;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.webServer = [GCDWebServer new];
    }
    
    return self;
}

- (NSString *)serverAddress {
    return [[self.webServer serverURL] absoluteString];
}

- (void)registerResponses:(NSArray<NSObject<N7WebResponse> *> *)responses {
    [responses enumerateObjectsUsingBlock:^(NSObject<N7WebResponse> * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSObject<N7WebResponse> *_obj = obj;
        
        [self.webServer addHandlerForMethod:[_obj method] path:[_obj path] requestClass:[GCDWebServerRequest class] asyncProcessBlock:^(GCDWebServerRequest *request, GCDWebServerCompletionBlock completionBlock) {
            completionBlock([_obj serverResponseForRequest:request]);
        }];
    }];
}

- (void)start {
    [self.webServer start];
}

- (void)stop {
    [self.webServer stop];
}

@end
