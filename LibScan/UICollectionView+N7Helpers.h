//
//  UICollectionView+N7Helpers.h
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionView (N7Helpers)

- (void)n7_registerNIBForCellClass:(Class)cellClass;

- (__kindof UICollectionViewCell *)n7_dequeueReusableCellWithClass:(Class)cellClass atIndexPath:(NSIndexPath *)indexPath;

@end
