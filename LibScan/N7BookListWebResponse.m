//
//  N7BookListWebResponse.m
//  LibScan
//
//  Created by Георгий Касапиди on 18.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

@import GCDWebServer;

#import "N7BookListWebResponse.h"
#import "N7BooksFileSystemManager.h"
#import "N7BookModel.h"

@implementation N7BookListWebResponse

- (NSString *)method {
    return @"GET";
}

- (NSString *)path {
    return @"/";
}

- (GCDWebServerResponse *)serverResponseForRequest:(GCDWebServerRequest *)request {
    NSArray<N7BookModel *> *books = [[N7BooksFileSystemManager manager] booksArray];
    
    if (books.count == 0) {
        return [GCDWebServerDataResponse response];
    }
    
    NSMutableArray<NSString *> *booksHTML = [NSMutableArray array];
    
    [books enumerateObjectsUsingBlock:^(N7BookModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *bookHTML = [NSString stringWithFormat:@"<tr><td><b>%@</b></td><td><i>%@</i></td><td><a href=\"book?id=%@\">%@</a></td></tr>", obj.name, obj.user, [obj.bookID UUIDString], NSLocalizedString(@"Скачать", nil)];
        
        [booksHTML addObject:bookHTML];
    }];
    
    NSString *booksHTMLString = [NSString stringWithFormat:@"<table>%@</table>", [booksHTML componentsJoinedByString:@""]];
    
    NSString *pageHTMLString = [NSString stringWithFormat:@"<html><head></head><body>%@</body></html>", booksHTMLString];
    
    return [GCDWebServerDataResponse responseWithHTML:pageHTMLString];
}

@end
