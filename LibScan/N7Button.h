//
//  N7Button.h
//  LibScan
//
//  Created by Георгий Касапиди on 16.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface N7Button : UIButton

@property (strong, nonatomic) IBInspectable UIColor *normalColor;
@property (strong, nonatomic) IBInspectable UIColor *selectedColor;

@end
