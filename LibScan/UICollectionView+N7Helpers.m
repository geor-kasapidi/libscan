//
//  UICollectionView+N7Helpers.m
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "UICollectionView+N7Helpers.h"

@implementation UICollectionView (N7Helpers)

- (void)n7_registerNIBForCellClass:(Class)cellClass {
    [self registerNib:[UINib nibWithNibName:NSStringFromClass(cellClass) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass(cellClass)];
}

- (UICollectionViewCell *)n7_dequeueReusableCellWithClass:(Class)cellClass atIndexPath:(NSIndexPath *)indexPath {
    return [self dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cellClass) forIndexPath:indexPath];
}

@end
