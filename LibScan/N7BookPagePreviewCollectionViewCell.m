//
//  N7BookPagePreviewCollectionViewCell.m
//  LibScan
//
//  Created by Георгий Касапиди on 18.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7BookPagePreviewCollectionViewCell.h"

@interface N7BookPagePreviewCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

@end

@implementation N7BookPagePreviewCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self addGestureRecognizer:[[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)]];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.previewImageView.transform = CGAffineTransformIdentity;
}

#pragma mark - Public

- (void)showPreviewImage:(UIImage *)previewImage withNumber:(NSInteger)number {
    self.previewImageView.image = previewImage;
    self.numberLabel.text = [@(number) stringValue];
}

#pragma mark - Gestures

- (void)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
        case UIGestureRecognizerStateChanged: {
            self.previewImageView.transform = CGAffineTransformMakeScale(MIN(recognizer.scale, 1), MIN(recognizer.scale, 1));
            
            break;
        }
        default: {
            if (recognizer.scale < 0.6) {
                self.previewImageView.transform = CGAffineTransformMakeScale(0, 0);
                
                [self.delegate bookPagePreviewCellDeletePage:self];
            } else {
                self.previewImageView.transform = CGAffineTransformIdentity;
            }
            
            break;
        }
    }
}

@end
