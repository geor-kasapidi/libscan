//
//  N7BookModel.m
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7BookModel.h"

@implementation N7BookModel

+ (instancetype)modelWithName:(NSString *)name user:(NSString *)user {
    return [self modelWithID:nil name:name user:user];
}

+ (instancetype)modelWithID:(NSUUID *)bookID name:(NSString *)name user:(NSString *)user {
    N7BookModel *model = [self new];
    
    model.bookID = bookID;
    model.name = name;
    model.user = user;
    
    return model;
}

#pragma mark - Equality

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if (![object isKindOfClass:[N7BookModel class]]) {
        return NO;
    }
    
    return [self.bookID isEqual:[(N7BookModel *)object bookID]];
}

- (NSUInteger)hash {
    return self.bookID.hash;
}

@end
