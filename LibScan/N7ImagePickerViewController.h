//
//  N7ImagePickerViewController.h
//  LibScan
//
//  Created by Георгий Касапиди on 16.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <UIKit/UIKit.h>

@class N7ImagePickerViewController;

@protocol N7ImagePickerViewControllerDelegate <NSObject>

@required

- (void)imagePicker:(N7ImagePickerViewController *)imagePicker savePhoto:(UIImage *)image preview:(UIImage *)preview;
- (void)imagePickerDismissed;

@end

@interface N7ImagePickerViewController : UIImagePickerController

@property (weak, nonatomic) id<N7ImagePickerViewControllerDelegate> pickerDelegate;

- (instancetype)initWithPreviewImage:(UIImage *)previewImage;

@end
