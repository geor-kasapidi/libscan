//
//  N7WebResponse.h
//  LibScan
//
//  Created by Георгий Касапиди on 18.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GCDWebServerResponse;
@class GCDWebServerRequest;

@protocol N7WebResponse <NSObject>

@required

- (NSString *)method;
- (NSString *)path;
- (GCDWebServerResponse *)serverResponseForRequest:(GCDWebServerRequest *)request;

@end
