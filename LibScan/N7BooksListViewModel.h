//
//  N7BooksListViewModel.h
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7ViewModel.h"

@class N7BookModel;
@class N7BooksListViewModel;

@protocol N7BooksListViewModelDelegate <NSObject>

@required

- (void)viewModel:(N7BooksListViewModel *)viewModel insertItemAtIndexPath:(NSIndexPath *)indexPath;
- (void)viewModel:(N7BooksListViewModel *)viewModel deleteItemAtIndexPath:(NSIndexPath *)indexPath;

- (void)viewModel:(N7BooksListViewModel *)viewModel configureItemAtIndexPath:(NSIndexPath *)indexPath;

- (void)viewModel:(N7BooksListViewModel *)viewModel setEmptyViewVisible:(BOOL)emptyViewVisible;

@end

@interface N7BooksListViewModel : N7ViewModel

@property (weak, nonatomic) id<N7BooksListViewModelDelegate> delegate; // view controller

- (void)activate;

- (NSString *)serverAddress;

- (NSInteger)numberOfBooks;
- (N7BookModel *)bookAtIndexPath:(NSIndexPath *)indexPath;

- (void)createOrUpdateBook:(N7BookModel *)book;
- (void)deleteBook:(N7BookModel *)book;

@end
