//
//  N7BooksListViewModel.m
//  LibScan
//
//  Created by Георгий Касапиди on 15.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7BooksListViewModel.h"
#import "N7BooksFileSystemManager.h"
#import "N7BookModel.h"
#import "N7BooksWebServer.h"

@interface N7BooksListViewModel ()

@property (strong, nonatomic) NSMutableArray *booksArray;

@end

@implementation N7BooksListViewModel

#pragma mark - Public

- (void)activate {
    NSMutableArray *booksArray = [NSMutableArray array];
    
    NSArray *books = [[N7BooksFileSystemManager manager] booksArray];
    
    if (books.count > 0) {
        [booksArray addObjectsFromArray:books];
    }
    
    self.booksArray = booksArray;
    
    [self.delegate viewModel:self setEmptyViewVisible:[self numberOfBooks] == 0];
}

- (NSString *)serverAddress {
    return [[N7BooksWebServer defaultServer] serverAddress];
}

- (NSInteger)numberOfBooks {
    return self.booksArray.count;
}

- (N7BookModel *)bookAtIndexPath:(NSIndexPath *)indexPath {
    NSParameterAssert(indexPath.length == 2);
    
    N7BookModel *book = self.booksArray[[indexPath indexAtPosition:1]];
    
    return book;
}

- (void)createOrUpdateBook:(N7BookModel *)book {
    if ([[N7BooksFileSystemManager manager] createOrUpdateBook:book]) {
        if (![self.booksArray containsObject:book]) {
            [self.booksArray addObject:book];
            
            [self.delegate viewModel:self insertItemAtIndexPath:[self indexPathForBook:book]];
            [self.delegate viewModel:self setEmptyViewVisible:[self numberOfBooks] == 0];
        } else {
            [self.delegate viewModel:self configureItemAtIndexPath:[self indexPathForBook:book]];
        }
    }
}

- (void)deleteBook:(N7BookModel *)book {
    if ([[N7BooksFileSystemManager manager] deleteBook:book]) {
        if ([self.booksArray containsObject:book]) {
            NSIndexPath *indexPath = [self indexPathForBook:book];
            
            [self.booksArray removeObject:book];
            
            [self.delegate viewModel:self deleteItemAtIndexPath:indexPath];
            [self.delegate viewModel:self setEmptyViewVisible:[self numberOfBooks] == 0];
        }
    }
}

#pragma mark - Private

- (NSIndexPath *)indexPathForBook:(N7BookModel *)book {
    NSUInteger indexOfBook = [self.booksArray indexOfObject:book];
    
    return indexOfBook != NSNotFound ? [[NSIndexPath indexPathWithIndex:0] indexPathByAddingIndex:indexOfBook] : nil;
}

@end
