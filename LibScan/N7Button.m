//
//  N7Button.m
//  LibScan
//
//  Created by Георгий Касапиди on 16.06.16.
//  Copyright © 2016 N7. All rights reserved.
//

#import "N7Button.h"

@implementation N7Button

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    self.backgroundColor = highlighted ? self.selectedColor : self.normalColor;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    self.backgroundColor = selected ? self.selectedColor : self.normalColor;
}

@end
